package com.sukhdevsingh2468.userauth;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;                                                        //Currently this project can upload data in firebase......
import com.google.firebase.database.ValueEventListener;                                                      //Need to create user authentication system by linking two activities....
//Secnd activity is yet yo be created
public class MainActivity extends AppCompatActivity {

    TextView detail1, detail2, detail3;
    Button signup, fetch;
    EditText username, email,search_name, phoneno;
    DatabaseReference root, current, newone, nnewone, USER, EMAIL, PHONE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        signup = (Button) findViewById(R.id.signup);
        fetch = (Button) findViewById(R.id.fetch);

        username = (EditText) findViewById(R.id.name);
        email = (EditText) findViewById(R.id.email);
        search_name = (EditText) findViewById(R.id.searchname);
        phoneno = (EditText) findViewById(R.id.Phone);

        detail1 = (TextView) findViewById(R.id.detail1);
        detail2 = (TextView) findViewById(R.id.detail2);
        detail3 = (TextView) findViewById(R.id.detail3);

        root = FirebaseDatabase.getInstance().getReference();

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String UN= username.getText().toString();
                String EM = email.getText().toString();
                String PN = phoneno.getText().toString();

                newone = root.child(UN);
                nnewone = newone.child("Username");
                nnewone.setValue(UN);
                nnewone = newone.child("Email");
                nnewone.setValue(EM);
                nnewone = newone.child("Phone-no");
                nnewone.setValue(PN);

            }
        });

        fetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String UN= search_name.getText().toString();
                current = FirebaseDatabase.getInstance().getReference(UN);
                USER = current.child("Username");
                PHONE = current.child("Phone-no");
                EMAIL = current.child("Email");

                USER.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String recivedusername = dataSnapshot.getValue(String.class);
                        if (recivedusername == null) {
                            detail1.setText("This is not a registered user");
                        } else {
                            detail1.setText("Username: " + recivedusername);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                PHONE.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String recivedphone = dataSnapshot.getValue(String.class);
                        if (recivedphone == null) {
                            detail3.setText("This is not a registered user");
                        } else {
                            detail3.setText("Phone no: " + recivedphone);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });

                EMAIL.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        String recivedemail = dataSnapshot.getValue(String.class);
                        if (recivedemail == null) {
                            detail2.setText("This is not a registered user");
                        } else {
                            detail2.setText("Email: " + recivedemail);
                        }
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

        });
    }
}